const localStorageService = {
    // Handle the task data
    save: (resource: string, item: string) =>
        localStorage.setItem(resource, item),
    get: (resource: string) => localStorage.getItem(resource),
};

const restAPIService = {};

interface IDBService {
    env: string;

    save: (resource: string, item: string) => void;
    get: (resource: string) => string;
}

class DBService implements IDBService {
    env: string;
    constructor(env: string) {
        this.env = env;
    }

    save = (resource: string, item: string) => {
        const dbService: any =
            this.env == "development" ? localStorageService : restAPIService;

        return dbService.save(resource, item);
    };

    get = (resource: string) => {
        const dbService: any =
            this.env == "development" ? localStorageService : restAPIService;

        return dbService.get(resource);
    };
}
export default DBService;
