import React from "react";
import "@testing-library/jest-dom/extend-expect";
import DBService from "..";
import { fireEvent, act, render } from "@testing-library/react";
import { BrowserRouter as Router } from "react-router-dom";

describe("DBService test", () => {

    describe("localStorage Service", () => {
        test("localStorage Service get method", () => {
            // 1. Simulate context ( Given )
            let dbService = new DBService("development");
            dbService.get = jest.fn();
            const spy = jest.spyOn(dbService, "get");
    
            // 2. Trigger method
            dbService.get("tasks");
    
            // 3. Expectation
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith("tasks");
        });

        test("localStorage Service save method", () => {
            // 1. Simulate context ( Given )
            let dbService = new DBService("development");
            dbService.save = jest.fn();
            const spy = jest.spyOn(dbService, "save");
    
            // 2. Trigger method
            dbService.save("tasks", "Do 1");
    
            // 3. Expectation
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith("tasks", "Do 1");
        });
    })
    describe("DB Service method", () => {
        test("DB Service save method", () => {
            // 1. Simulate context ( Given )
            let dbService = new DBService("development");
            dbService.save = jest.fn();
            const spy = jest.spyOn(dbService, "save");
    
            // 2. Trigger method
            dbService.save("apple-iphone", "10000");
    
            // 3. Expectation
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith("apple-iphone","10000");
        });
        
        test("DB Service get method", () => {
            // 1. Simulate context ( Given )
            let dbService = new DBService("development");
            dbService.get = jest.fn();
            const spy = jest.spyOn(dbService, "get");
    
            // 2. Trigger method
            dbService.get("tasks");
    
            // 3. Expectation
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith("tasks");
        });
    }) 
    

    
});
