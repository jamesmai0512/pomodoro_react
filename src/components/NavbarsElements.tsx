import { NavLink, Link } from "react-router-dom";
import styled from "styled-components";

export const NavLinkCom = styled(NavLink)``;

export const NavBtn = styled(Link)`
    text-decoration: none;
    color: rgb(183, 211, 186);
    font-family: "quicksand", "cursive";
    font-weight: monospace;
    font-size: 25px;
    transition: 1s;
    &:hover {
        transition: 1s;
        background: rgba(255, 255, 255, 0.7);
        color: #0dab9e;
    }
`;
