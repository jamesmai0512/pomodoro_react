import React from "react";
import "@testing-library/jest-dom/extend-expect";
import Header from "../";
import { fireEvent, act, render } from "@testing-library/react";
import { BrowserRouter as Router } from "react-router-dom";

describe("Header test", () => {
    test("Header without signed in", () => {
        const { getByText } = render(
            <Router>
                <Header authorized={false} />
            </Router>
        );

        expect(getByText(/Sign Up/i)).toBeInTheDocument();
        expect(getByText(/Sign In/i)).toBeInTheDocument();
    });
});
