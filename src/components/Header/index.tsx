import React, { useEffect, useState } from "react";
import { NavLinkCom, NavBtn } from "../NavbarsElements";
import "../../styles/header.css";
import DBService from  "../../service/DBService"
import { BiLogOut } from "react-icons/bi";

interface IUser {
    id: number;
    email: string;
    user_name: string;
}

interface IProps {
    authorized: boolean;
}

function Header({ authorized }: IProps) {
    const newDBService = new DBService("development");
    const [isAuthenticate, setIsAuthenticate] = useState<boolean>();
    const [user, setUser] = useState<IUser>();

    useEffect(() => {
        if (authorized) {
            setIsAuthenticate(authorized);
            setUser(JSON.parse(newDBService.get("account") || ""));
        }                                  
    }, []);

    const logout = () => {
        localStorage.removeItem("account");
        setIsAuthenticate(false);
        location.reload();
    };

    return (
        <>
            <div className="wrap">
                <ul className="nav">
                    <li>
                        <NavLinkCom to="/">Pomodoro</NavLinkCom>
                    </li>
                    {isAuthenticate ? (
                        <button className="logout" onClick={logout}>
                            <BiLogOut /> Logout
                        </button>
                    ) : (
                        <li>
                            <NavLinkCom to="/signup">Sign Up</NavLinkCom>
                        </li>
                    )}
                </ul>
                <div className="info">
                    {isAuthenticate ? (
                        user!.user_name
                    ) : (
                        <NavBtn to="/signin">Sign In</NavBtn>
                    )}
                </div>
            </div>
        </>
    );
}

export default Header;
