import React from "react";
import { useNavigate } from "react-router-dom";
import { IoMdArrowRoundBack } from "react-icons/io";
import "../styles/header.css";

function BackButton() {
    const navigate = useNavigate();

    return (
        <span className="back-button" onClick={() => navigate(-1)}>
            <IoMdArrowRoundBack />
        </span>
    );
}

export default BackButton;
