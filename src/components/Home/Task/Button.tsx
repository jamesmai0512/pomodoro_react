import React from "react";

interface IButton {
    id: number;
    className: string;
    // Label of button
    label: string;
}

function Button({ id, className = "control-task-btn", label }: IButton) {
    return (
        <button data-id={id} className={className}>
            {label}
        </button>
    );
}

export default Button;
