import React from "react";
import { ITask } from "./index";
import Button from "./Button";
interface Props {
    task: ITask;
}

const Task: React.FC<Props> = ({ task }: Props) => {
    return (
        <tr>
            <td className="cell-task-name">{task.task_name}</td>
            <td className="cell-pom-count">
                {task.pomodoro_done} Pomodori
            </td>
            <td className="cell-pom-controls">
                {task.finished ? (
                    "Finished"
                ) : (
                    <>
                        <Button
                            id={task.id}
                            className={"js-status-task control-task-btn"}
                            label={"Increase Pomodoro Count"}
                        />
                        <Button
                            id={task.id}
                            className={"js-task-done control-task-btn"}
                            label={"Done"}
                        />
                    </>
                )}
                <Button
                    id={task.id}
                    className={"js-delete-task control-task-btn"}
                    label={"Delete"}
                />
            </td>
        </tr>
    );
};

export default Task;
