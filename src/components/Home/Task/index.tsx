export interface ITask {
    user_id?: number;
    id: number;
    task_name: string;
    pomodoro_done: number;
    finished: boolean;
}

export interface IUser {
    id: number;
    email: string;
    user_name: string;
}
