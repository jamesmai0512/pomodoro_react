import React, { useState, MouseEvent, useEffect, useRef } from "react";
import DBService from "../../../service/DBService";
import TaskTable from "./TaskTable";
import { ITask, IUser } from "./index";

interface IProps {
    authorized: boolean;
    userId: number;
}

const TaskForm: React.FC<IProps> = ({ authorized, userId }) => {
    const newDBService = new DBService("development");
    const [tasks, setTasks] = useState<ITask[]>(
        JSON.parse(newDBService.get("tasks") || "[]")
    ); //Get Tasks // JSON.parse() to convert text into a JavaScript object:
    const [tasksUser, setTasksUser] = useState<ITask[]>(
        tasks.filter((task) => task.user_id == userId)
    );
    const [name, setName] = useState<string>("");

    const handleSubmitTask = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        if (!name || /^\s+$/.test(name)) {
            return;
        }
        let task: ITask = {
            user_id: authorized ? userId : 0,
            id: tasks.length > 0 ? tasks[tasks.length - 1].id + 1 : 1,
            task_name: name,
            pomodoro_done: 0,
            finished: false,
        };

        setTasks((tasks) => [...tasks, task]);
        setName("");
    };

    const handleTaskButtonClick = (event: MouseEvent) => {
        const taskTarget = event.target! as HTMLElement;
        console.log(taskTarget);

        const taskId = taskTarget.dataset.id!;
        switch (true) {
            case taskTarget.matches(".js-status-task"):
                taskCount(taskId);
                break;
            case taskTarget.matches(".js-task-done"):
                finishedTask(taskId);
                break;
            case taskTarget.matches(".js-delete-task"):
                deleteOneById(taskId);
                break;
        }
    };
    const taskCount = (id: string) => {
        const taskIndex: number = tasks.findIndex((e) => e.id == +id);
        tasks[taskIndex].pomodoro_done += 1;
        setTasks((tasks) => [...tasks]);
    };

    const finishedTask = (id: string) => {
        const taskIndex: number = tasks.findIndex((e) => e.id == +id);
        tasks[taskIndex].finished = true;
        setTasks((tasks) => [...tasks]);
    };

    const deleteOneById = (id: string) => {
        const newArray = tasks.filter((task: ITask) => task.id != +id);

        setTasks((tasks) => newArray);
    };

    // Save Data to db
    useEffect(() => {
        newDBService.save("tasks", JSON.stringify(tasks)); // Use JSON.stringify() to convert the JavaScript object into a string
        setTasksUser(tasks.filter((task: ITask) => task.user_id == userId));
    }, [tasks]);

    return (
        <>
            <div className="form-add-task-{}">
                <form className="js-add-task-{}" onSubmit={handleSubmitTask}>
                    <fieldset className="add-task-fieldset">
                        <input
                            value={name}
                            onChange={(e) => setName(e.target.value)}
                            className="js-add-task-name js-add-task-name-{}"
                            type="text"
                            placeholder="Task Name"
                        />
                        <button className="btn add-task-btn" type="submit">
                            Add
                        </button>
                    </fieldset>
                </form>
            </div>

            {/* Table Tasks */}
            <TaskTable
                tasks={tasksUser}
                handleTaskButtonClick={handleTaskButtonClick}
            />
        </>
    );
};

export default TaskForm;
