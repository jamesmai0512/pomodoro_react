import React, { MouseEvent } from "react";
import Task from "./TaskItem";
import { ITask } from "./index";
interface Props {
    tasks: ITask[];
    handleTaskButtonClick: (event: MouseEvent) => void;
}
const TaskTable = ({ tasks, handleTaskButtonClick }: Props) => {
    return (
        <div className="table-task">
            <table>
                <thead>
                    <tr>
                        <th>Task Name</th>
                        <th>Status</th>
                        <th>Controls</th>
                    </tr>
                </thead>

                {/* Table body   */}
                <tbody
                    className="js-task-table-body"
                    onClick={handleTaskButtonClick}
                >
                    {/* <Task /> */}
                    {tasks.map((task) => (
                        <Task key={task.id} task={task} />
                    ))}
                </tbody>
            </table>
        </div>
    );
};

export default TaskTable;
