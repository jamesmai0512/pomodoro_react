import React, { MouseEvent } from "react";

interface Props {
    handleStartTimer: () => void;
    handleStopTimer: () => void;
}

const PromodoroTimerButton = ({ handleStartTimer, handleStopTimer }: Props) => {
    return (
        <div className="control-btn-timer control-btn-timer-{}">
            <div id="btn-{}">
                <button
                    onClick={handleStartTimer}
                    className="btn start-btn start-btn-{}"
                >
                    start
                </button>
                <button onClick={handleStopTimer} className="stop-btn hidden">
                    stop
                </button>
                <a href="">
                    <i className="fas fa-redo-alt" aria-hidden="true"></i>
                </a>
            </div>
        </div>
    );
};

export default PromodoroTimerButton;
