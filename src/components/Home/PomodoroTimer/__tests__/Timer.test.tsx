import React from "react";
import "@testing-library/jest-dom/extend-expect";
import Timer from "../Timer";
import { render } from "@testing-library/react";

describe("Timer Ui", () => {
    test("default Timer", () => {
        const { getByText } = render(<Timer minute={22} second={30} />);
        expect(getByText(/22:30/i)).toBeInTheDocument();
    });

    test("Timer under 10 minutes", () => {
        const { getByText } = render(<Timer minute={9} second={30} />);
        expect(getByText(/09:30/i)).toBeInTheDocument();
    });

    test("Timer with negative number", () => {
        const { getByText } = render(<Timer minute={-2} second={-99} />);
        expect(getByText(/25:00/i)).toBeInTheDocument();
    });

    test("Timer with big number", () => {
        const { getByText } = render(<Timer minute={100} second={123123} />);
        expect(getByText(/25:00/i)).toBeInTheDocument();    
    });
});
