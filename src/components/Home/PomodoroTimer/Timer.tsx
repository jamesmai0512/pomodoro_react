import React from "react";

interface Props {
    second: number;
    minute: number;
}

const Timer = ({ second, minute }: Props) => {
    // Check negative number
    minute = minute > 0 ? minute : 25;
    second = second > 0 ? second : 0;

    // Check number more than default number
    // minute has to <= 25
    minute = minute > 25 ? 25 : minute;
    // second has to <= 60
    second = second > 60 ? 0 : second;

    const timerMinute: string = minute < 10 ? `0${minute}` : `${minute}`;

    const timerSecond: string = second < 10 ? `0${second}` : `${second}`;

    return (
        <div className="timer-detail">
            <h4>
                {timerMinute}:{timerSecond}
            </h4>
        </div>
    );
};

export default Timer;
