import React, { useState, useEffect, useRef } from "react";
import PromodoroTimerButton from "./PromodoroTimerButton";
import Timer from "./Timer";

const PomodoroTimer = () => {
    const [secondsLeft, setSecondsLeft] = useState<number>(25 * 60);

    const timerInterval = useRef(0);

    const handleStartTimer = () => {
        let startButton = document.querySelector(
            ".start-btn"
        ) as unknown as HTMLElement;
        let stopButton = document.querySelector(
            ".stop-btn"
        ) as unknown as HTMLElement;

        startButton.classList.add("hidden");
        stopButton.classList.remove("hidden");
        timerInterval.current = window.setInterval(() => {
            setSecondsLeft((secondsLeft) => secondsLeft - 1);

            if (secondsLeft === 0) {
                clearInterval(timerInterval.current);
            }
        }, 1000);
    };

    const handleStopTimer = () => {
        let startButton = document.querySelector(
            ".start-btn"
        ) as unknown as HTMLElement;
        let stopButton = document.querySelector(
            ".stop-btn"
        ) as unknown as HTMLElement;

        startButton.classList.remove("hidden");
        stopButton.classList.add("hidden");
        clearInterval(timerInterval.current);
    };

    // second === 0 => stop
    useEffect(() => {
        if (secondsLeft === 0) {
            clearInterval(timerInterval.current);
            setSecondsLeft(25 * 60);
        }
    }, [secondsLeft, timerInterval]);

    // component unmount => stop, then it will clear interval
    useEffect(() => {
        return () => clearInterval(timerInterval.current);
    }, [timerInterval]);

    // Divide 60 to get minutes
    const minutesFromSeconds = Math.floor(secondsLeft / 60);
    // Percentage to get seconds
    const remainingSeconds = secondsLeft % 60;

    return (
        <>
            {/* Button Control Pomodoro Timer */}
            <PromodoroTimerButton
                handleStartTimer={handleStartTimer}
                handleStopTimer={handleStopTimer}
            />

            <div id="pomodoro-timer-clock-{}">
                <div id="{}">
                    <div className="pomodoro-timer">
                        <div className="timer">
                            {/* Timer Component */}
                            <Timer
                                second={remainingSeconds}
                                minute={minutesFromSeconds}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default PomodoroTimer;
