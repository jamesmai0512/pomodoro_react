import React from "react";

interface ClockProps {}

interface ClockState {
    date: string;
}

class Clock extends React.Component<ClockProps, ClockState> {
    interval: NodeJS.Timer;
    constructor(props: ClockProps) {
        super(props);
        this.updateDate = this.updateDate.bind(this);

        this.state = {
            date: new Date().toLocaleTimeString("en-GB"),
        };

        this.interval = setInterval(this.updateDate, 1000);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    updateDate() {
        this.setState({
            date: new Date().toLocaleTimeString("en-GB"),
        });
    }

    render() {
        return (
            <div className="clock-timer clock-timer-{}">
                <div id="clock-{}" className="clock clock-display">
                    {this.state.date}
                </div>
            </div>
        );
    }
}

export default Clock;
