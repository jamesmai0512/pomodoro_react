import { useState, useRef } from "react";

import Clock from "../../components/Home/Clock";
import PromodoroTimer from "../../components/Home/PomodoroTimer/PomodoroTimer";
import TaskForm from "../../components/Home/Task/Task";
import Header from "../../components/Header";
import "../../styles/home.css";
import "../../styles/index.css";

function Home() {
    const authRef = useRef(false);
    if (localStorage.getItem("account") != null) {
        authRef.current = true;
        var userId: number = JSON.parse(
            localStorage.getItem("account") || "{}"
        ).id;
    } else {
        var userId: number = 0;
    }

    return (
        <div className="Home">
            {/* Header */}
            <Header authorized={authRef.current} />

            {/* Clock */}
            <Clock />

            {/* Pomodoro Timer */}
            <PromodoroTimer />

            {/* Form add task to Table Body below */}
            <TaskForm authorized={authRef.current} userId={userId} />
        </div>
    );
}

export default Home;
