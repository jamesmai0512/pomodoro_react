export interface IFormData {
    user_name?: string; //optional
    email: string; // required
    password: string; //required
}
