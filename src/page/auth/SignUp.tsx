import React, { useState, useEffect, MouseEvent } from "react";
import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import { IFormData } from "./index";
import BackButton from "../../components/BackButton";
import { FaUserCircle } from "react-icons/fa";
import "../../styles/authentication.css";

const SignUp = () => {
    let navigate = useNavigate();
    useEffect(() => {
        if (localStorage.getItem("account") != null) {
            navigate("/");
        }
    }, []);

    const formik = useFormik({
        initialValues: {
            user_name: "",
            email: "",
            password: "",
            confirmedPassword: "",
        },
        validationSchema: Yup.object({
            user_name: Yup.string().required("Required").min(2, "Too Short"),
            email: Yup.string().required("Required"),
            password: Yup.string()
                .required("Required")
                .min(
                    5,
                    "The Password is too short, need more than 5 characters"
                ),
            confirmedPassword: Yup.string()
                .required("Required")
                .oneOf([Yup.ref("password"), null], "Password must match"),
        }),

        onSubmit: (value) => {
            fetch("http://localhost:3001/users", {
                method: "POST",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify(value),
            }).then((res) => {
                res.json, navigate("/signin");
            });

            formik.resetForm();
        },
    });

    return (
        <>
            <BackButton />
            <div className="container">
                <div className="container-wrapper">
                    <h3 className="login-text">
                        <FaUserCircle /> Sign Up
                    </h3>
                    <form onSubmit={formik.handleSubmit} action="">
                        <div className="item">
                            <label htmlFor="user_name">
                                Username
                                <input
                                    type="text"
                                    value={formik.values.user_name}
                                    onChange={formik.handleChange}
                                    placeholder="Username"
                                    id="user_name"
                                    name="user_name"
                                    className="input"
                                />
                            </label>
                            {formik.errors.user_name && (
                                <p className="errorMsg">
                                    {formik.errors.user_name}
                                </p>
                            )}
                        </div>
                        <div className="item">
                            <label htmlFor="email">
                                Email
                                <input
                                    type="email"
                                    value={formik.values.email}
                                    onChange={formik.handleChange}
                                    placeholder="Email"
                                    id="email"
                                    name="email"
                                    className="input"
                                />
                            </label>

                            {formik.errors.email && (
                                <p className="errorMsg">
                                    {formik.errors.email}
                                </p>
                            )}
                        </div>
                        <div className="item">
                            <label htmlFor="password">
                                Password
                                <input
                                    type="password"
                                    value={formik.values.password}
                                    onChange={formik.handleChange}
                                    placeholder="Password"
                                    id="password"
                                    name="password"
                                    className="input"
                                />
                            </label>

                            {formik.errors.password && (
                                <p className="errorMsg">
                                    {formik.errors.password}
                                </p>
                            )}
                        </div>

                        <div className="item">
                            <label htmlFor="confirmedPassword">
                                Confirm Password
                                <input
                                    type="password"
                                    value={formik.values.confirmedPassword}
                                    onChange={formik.handleChange}
                                    placeholder="Confirm Password"
                                    id="confirmedPassword"
                                    name="confirmedPassword"
                                    className="input"
                                />
                            </label>

                            {formik.errors.confirmedPassword && (
                                <p className="errorMsg">
                                    {formik.errors.confirmedPassword}
                                </p>
                            )}
                        </div>

                        <div className="item submit">
                            <button name="sign-up" type="submit">
                                Submit
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </>
    );
};

export default SignUp;
