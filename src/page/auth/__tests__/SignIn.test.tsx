import React from "react";
import "@testing-library/jest-dom/extend-expect";
import SignIn from "../SignIn";
import DBService from "../../../service/DBService";
import { fireEvent, act, render, waitFor } from "@testing-library/react";
import { BrowserRouter as Router, useNavigate } from "react-router-dom";
import signInApi from "../signInApi";

jest.mock("../signInApi");

const mockSignIn = {
    data: {
        user: {
            email: "1111@gmail.com",
        },
    },
};

// mock useNavigate
const mockedUsedNavigate = jest.fn();

jest.mock("react-router-dom", () => ({
    ...jest.requireActual("react-router-dom"),
    useNavigate: () => mockedUsedNavigate,
}));

describe("SignIn Component", () => {
    describe("SignIn render", () => {
        test("UI should be render", () => {
            // 1. Simulate context ( Given )
            const { getByText } = render(
                <Router>
                    <SignIn />
                </Router>
            );

            // 2. Expectation
            expect(getByText(/Email/i)).toBeInTheDocument();
            expect(getByText(/Password/i)).toBeInTheDocument();
            expect(getByText(/Submit/i)).toBeInTheDocument();
        });

        test("Pass valid email to test email input", async () => {
            // 1. Simulate context ( Given )
            const { getByLabelText, getByText } = render(
                <Router>
                    <SignIn />
                </Router>
            );

            fireEvent.change(getByLabelText(/Email/i), {
                target: { value: "1111@gmail.com" },
            });

            // 2. Expectation
            await waitFor(() => {
                expect(getByLabelText(/Email/i)).toHaveValue("1111@gmail.com");
            });
        });

        test("Pass an invalid email to test email input", async () => {
            // 1. Simulate context ( Given )
            const { getByLabelText, getByText, getByTestId } = render(
                <Router>
                    <SignIn />
                </Router>
            );

            fireEvent.change(getByLabelText(/Email/i), {
                target: { value: "111" },
            });

            // 2. Expectation
            await waitFor(() => {
                expect(
                    getByText(/Please enter a valid email address/i)
                ).toBeInTheDocument();
            });
        });

        test("Pass an invalid password to test password input", async () => {
            // 1. Simulate context ( Given )
            const { getByLabelText, getByText } = render(
                <Router>
                    <SignIn />
                </Router>
            );

            fireEvent.change(getByLabelText(/Password/i), {
                target: { value: "111" },
            });

            // 2. Expectation
            await waitFor(() => {
                expect(
                    getByText(/Must be more than five characters/i)
                ).toBeInTheDocument();
            });
        });

        test("empty input and click submit", async () => {
            // 1. Simulate context ( Given )
            const { getByTestId, getAllByText } = render(
                <Router>
                    <SignIn />
                </Router>
            );

            // 2. Trigger click
            fireEvent.click(getByTestId("signin-btn"));

            // 3. Expectation
            await waitFor(() => {
                expect(getAllByText(/Required/i)).toBeTruthy();
            });
        });
    });
    describe("SignIn success", () => {
        beforeEach(() => {
            signInApi.mockResolvedValueOnce(mockSignIn);
            // Mock resolve is a promise resolve
        });

        test("Success to Sign In with correct email and password", async () => {
            // 1. Simulate context ( Given )
            jest.spyOn(window.localStorage.__proto__, "setItem");
            window.localStorage.__proto__.setItem = jest.fn();

            const { getByLabelText, getByTestId } = render(
                <Router>
                    <SignIn />
                </Router>
            );

            fireEvent.change(getByLabelText(/Email/i), {
                target: { value: "1111@gmail.com" },
            });
            fireEvent.change(getByLabelText(/Password/i), {
                target: { value: "123123" },
            });

            // 2. Trigger click
            fireEvent.click(getByTestId("signin-btn"));

            // 3. Expectation
            await waitFor(() => {
                expect(localStorage.setItem).toHaveBeenCalledTimes(1);
                expect(localStorage.setItem).toHaveBeenCalledWith(
                    "account",
                    '{"email":"1111@gmail.com"}'
                );
                expect(mockedUsedNavigate).toHaveBeenCalledWith("/");
            });
        });
    });

    describe("Signin fail", () => {
        beforeEach(() => {
            signInApi.mockRejectedValue("error");
            // Mock reject is a promise reject
        });

        test("Fail to Sign In with the wrong email and password", async () => {
            // GIVEN
            const { getByLabelText, getByTestId } = render(
                <Router>
                    <SignIn />
                </Router>
            );

            fireEvent.change(getByLabelText(/Email/i), {
                target: { value: "b@b1.com" },
            });
            fireEvent.change(getByLabelText(/Password/i), {
                target: { value: "11111" },
            });
            fireEvent.click(getByTestId("signin-btn"));

            // works:
            jest.spyOn(window.localStorage.__proto__, "setItem");
            window.localStorage.__proto__.setItem = jest.fn();

            await waitFor(() => {
                expect(mockedUsedNavigate).not.toHaveBeenCalledWith("/");
                expect(localStorage.setItem).not.toHaveBeenCalled();
                expect(getByTestId("signin-email")).toHaveValue("");
                expect(getByTestId("signin-password")).toHaveValue("");
            });
        });
    });

    describe("Signin style test", () => {
        test("test style of signin text", () => {
            // 1. Simulate context ( Given )
            const { getByText } = render(
                <Router>
                    <SignIn />
                </Router>
            );
            const signInText = getByText("Sign in");

            expect(signInText).toHaveStyle(`
                font-size: 1.17em
            `);
        });
    });
});
