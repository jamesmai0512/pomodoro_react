import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import axios from "axios";
import BackButton from "../../components/BackButton";
import { FaUserAlt } from "react-icons/fa";
import "../../styles/authentication.css";
import signInApi from "./signInApi";

const SignIn = () => {
    let navigate = useNavigate();

    useEffect(() => {
        if (localStorage.getItem("account") != null) {
            navigate("/");
        }
    }, []);

    const formik = useFormik({
        initialValues: {
            email: "",
            password: "",
        },
        validationSchema: Yup.object({
            email: Yup.string()
                .required("Required")
                .matches(
                    /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/,
                    "Please enter a valid email address"
                ),
            password: Yup.string()
                .required("Required")
                .min(5, "Must be more than five characters"),
        }),

        onSubmit: async (value) => {
            try {
                const result = await signInApi(value);
                console.log("restuls ----", result);
                const account = JSON.stringify(result.data.user);
                localStorage.setItem("account", account);
                navigate("/");
            } catch (error) {
                console.log("Catching authenticate error:", error);
                formik.resetForm();
                return error;
            }
        },
    });

    return (
        <>
            <BackButton />
            <div  className="container">
                <div className="container-wrapper">
                    <h3 className="login-text">
                        <FaUserAlt /> Sign in
                    </h3>
                    <form onSubmit={formik.handleSubmit} action="">
                        <div className="item">
                            <label htmlFor="email">
                                Email
                                <input
                                    data-testid="signin-email"
                                    type="email"
                                    value={formik.values.email}
                                    onChange={formik.handleChange}
                                    placeholder="Email"
                                    id="email"
                                    name="email"
                                    className="input"
                                />
                                {formik.errors.email && (
                                    <p className="errorMsg">
                                        {formik.errors.email}
                                    </p>
                                )}
                            </label>
                        </div>
                        <div className="item">
                            <label htmlFor="password">
                                Password
                                <input
                                    data-testid="signin-password"
                                    type="password"
                                    value={formik.values.password}
                                    onChange={formik.handleChange}
                                    placeholder="Password"
                                    id="password"
                                    name="password"
                                    className="input"
                                />
                                {formik.errors.password && (
                                    <p className="errorMsg">
                                        {formik.errors.password}
                                    </p>
                                )}
                            </label>
                        </div>

                        <div className="item submit">
                            <button data-testid="signin-btn" type="submit">
                                Submit
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </>
    );
};

export default SignIn;
