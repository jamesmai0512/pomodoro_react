import axios from "axios";
interface IValue {
    email: string;
    password: string;
}

const signInApi = (value: IValue) =>
    axios.post("http://localhost:3001/login", value);

export default signInApi;
