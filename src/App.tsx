import { Routes, Route } from "react-router-dom";
import Home from "./page/Home";
import SignIn from "./page/auth/SignIn";
import SignUp from "./page/auth/SignUp";

function App() {
    return (
        <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/signin" element={<SignIn />} />
            <Route path="/signup" element={<SignUp />} />
        </Routes>
    );
}

export default App;
