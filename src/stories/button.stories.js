import React from "react";
import Button from "../components/Home/Task/Button";
import { withInfo } from "@storybook/addon-info";
import "../styles/home.css";

export default {
    title: "Button",
    component: Button,
    decorators: [withInfo],
    // argTypes: { onClick: { action: "clicked" } },
};

const Template = (args) => <Button {...args} />;

export const Status = Template.bind({});
Status.args = {
    id: 1,
    className: "js-status-task control-task-btn",
    label: "Increase Pomodoro Count",
};

export const Done = Template.bind({});
Done.args = {
    id: 1,
    className: "js-task-done control-task-btn",
    label: "Done",
};

export const Delete = Template.bind({});
Delete.args = {
    id: 1,
    className: "js-delete-task control-task-btn",
    label: "Delete",
};
