In the project directory, you can run:

### `pnpm install`

To install node module

### `pnpm dev`

Open [http://localhost:3000] to view it in the browser.

The page will reload if you make edits. <br>

To run the JSON Server

### `pnpm start`

Open [http://localhost:3001] to view it in the browser.

### `pnpm run storybook`

Show story book

To test the react component
### `pnpm test`
